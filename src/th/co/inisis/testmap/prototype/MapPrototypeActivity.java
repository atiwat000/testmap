package th.co.inisis.testmap.prototype;

import th.co.inisis.testmap.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapPrototypeActivity extends Activity implements OnMarkerDragListener, OnMarkerClickListener,
		OnMapLongClickListener {

	private GoogleMap mMap;
	protected Marker locMarker;
	private boolean isMapChanged = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mMap = getMap();
		mMap.setMyLocationEnabled(true);
		mMap.setOnMarkerDragListener(this);
		mMap.setOnMarkerClickListener(this);
		mMap.setOnMapLongClickListener(this);
	}

	public GoogleMap getMap() {
		return ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
	}

	public LatLng getMyPosition() {
		LatLng myPosition = null;
		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		String provider = locationManager.getBestProvider(criteria, true);
		Location location = locationManager.getLastKnownLocation(provider);
		if (location != null) {
			double latitude = location.getLatitude();
			double longitude = location.getLongitude();
			myPosition = new LatLng(latitude, longitude);
		}
		return myPosition;
	}

	public void setMarker(LatLng location){
		if (location != null) {
			locMarker = getMap().addMarker(
					new MarkerOptions().position(location).title(getString(R.string.loc_title)).draggable(true).snippet(getString(R.string.loc_snippet)));

			// map.moveCamera(CameraUpdateFactory.newLatLngZoom(MYHOME, (float)
			// 21.0));
			getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(location, (float) 17.0), 2000, null);
		}else{
			getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(getMyPosition(), (float) 17.0), 2000, null);
			Toast.makeText(this, getString(R.string.loc_unset), Toast.LENGTH_LONG).show();
		}
	}
	public boolean isMapChanged(){
		return this.isMapChanged;
	}
	private void showGPSDisabledAlertToUser() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setMessage(getString(R.string.gps_dialog)).setCancelable(false)
				.setPositiveButton(getString(R.string.gps_enabling), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent callGPSSettingIntent = new Intent(
								android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						startActivity(callGPSSettingIntent);
					}
				});
		alertDialogBuilder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = alertDialogBuilder.create();
		alert.show();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		int PlayCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		switch (PlayCode) {
		case ConnectionResult.SUCCESS:
			break;
		case ConnectionResult.SERVICE_MISSING:
		case ConnectionResult.SERVICE_INVALID:
		case ConnectionResult.SERVICE_DISABLED:
		case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
			GooglePlayServicesUtil.getErrorDialog(PlayCode, this, 0).show();
			break;
		}

		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

		if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			// Toast.makeText(this, getString(R.string.gps_ok),
			// Toast.LENGTH_SHORT).show();
		} else {
			showGPSDisabledAlertToUser();
		}
	}

	@Override
	public void onMarkerDrag(Marker arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMarkerDragEnd(Marker arg0) {
		// TODO Auto-generated method stub
		this.isMapChanged = true;
		Toast.makeText(this, getString(R.string.loc_change), Toast.LENGTH_SHORT).show();
		Log.d("LocChange", " [lat:" + getMap().getMyLocation().getLatitude() + ",lng:"
				+ getMap().getMyLocation().getLongitude() + "]");
	}

	@Override
	public void onMarkerDragStart(Marker arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean onMarkerClick(Marker arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onMapLongClick(LatLng arg0) {
		// TODO Auto-generated method stub

		if (getMap() != null && locMarker == null) {
			this.isMapChanged = true;
			locMarker = getMap().addMarker(
					new MarkerOptions().position(arg0).title(getString(R.string.loc_title)).draggable(true)
							.snippet(getString(R.string.loc_snippet))
							.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
			Toast.makeText(this, getString(R.string.loc_set), Toast.LENGTH_SHORT).show();
			Log.d("LocChange", " [lat:" + getMap().getMyLocation().getLatitude() + ",lng:"
					+ getMap().getMyLocation().getLongitude() + "]");
		}
	}

}
